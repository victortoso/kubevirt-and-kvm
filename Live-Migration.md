# Live Migration

There are two scenarios where live migration is triggered in KubeVirt

* As per user request, by posting a `VirtualMachineInstanceMigration`
  to the cluster
* As per cluster request, for instance on a Node eviction (due lack
  of resources or maintenance of given Node)

In both situations, KubeVirt will use libvirt to handle logic and
coordination with QEMU while KubeVirt's components manage the
Kubernetes control plane and the cluster's limitations.

In short, KubeVirt:

* Checks if the target host is capable of migration of the given VM;
* Handles single network namespace per Pod by proxying migration data
  (more at [Networking][])
* Handles cluster resources usage (e.g: bandwidth usage);
* Handles cross-version migration;

![Live migration between two nodes][Live-Migration-Flow]

## Limitations

Live migration is not possible if:

* The VM is configured with cpu-passthrough;
* The VM has local or non-shared volume
* The Pod is using bridge binding for network access (right side of
  image below)

![Kubevirt's Pod][Live-Migration-Network]

## More on KubeVirt's Live migration

This blog [post on live migration][] explains how to have live
migration enabled in KubeVirt's VMs and describes some of its
caveats.

[Live-Migration-Flow]: Images/Live-Migration-Flow.png
[Live-Migration-Network]: Images/Live-Migration-Network.png
[Networking]: Networking.md
[post on live migration]: https://kubevirt.io/2020/Live-migration.html
